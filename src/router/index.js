import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/pages/Dashboard'
import Search from '@/pages/Search'
import Appointments from '@/pages/Appointments/Appointments'
import BookAppointment from '@/pages/Appointments/BookAppointment'
import ConfirmBooking from '@/pages/Appointments/ConfirmBooking'
import Me from '@/pages/Me'
import PersonalInfo from '@/pages/PersonalInfo'
import Payment from '@/pages/Payment'
import Settings from '@/pages/Settings'
import Practice from '@/pages/Practice'
import AvailableTimes from '@/pages/AvailableTimes/AvailableTimes'
import AddAvailableTimes from '@/pages/AvailableTimes/AddAvailableTimes'
import FeeTable from '@/pages/FeeTable/FeeTable'
import AddFeeItem from '@/pages/FeeTable/AddFeeItem'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/search',
      name: 'Search',
      component: Search
    },
    {
      path: '/appointments',
      name: 'Appointments',
      component: Appointments
    },
    {
      path: '/appointments/book-appointment',
      name: 'Book Appointment',
      component: BookAppointment
    },
    {
      path: '/appointments/confirm-booking',
      name: 'Confirm Booking',
      component: ConfirmBooking
    },
    {
      path: '/me',
      name: 'Me',
      component: Me
    },
    {
      path: '/personal-info',
      name: 'PersonalInfo',
      component: PersonalInfo
    },
    {
      path: '/payment',
      name: 'Payment',
      component: Payment
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    },
    {
      path: '/practice-info',
      name: 'Practice',
      component: Practice
    },
    {
      path: '/available-times',
      name: 'Available Times',
      component: AvailableTimes
    },
    {
      path: '/available-times/add-available-times',
      name: 'Add Available Times',
      component: AddAvailableTimes
    },
    {
      path: '/fee-table',
      name: 'Fee Table',
      component: FeeTable
    },
    {
      path: '/fee-table/add-fee-item',
      name: 'Add Fee Item',
      component: AddFeeItem
    }
  ]
})
