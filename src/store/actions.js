import * as firebase from 'firebase'
import router from '../router'
const moment = require('moment')
export default {

  autoSignIn ({ commit, dispatch }, payload) {
    dispatch('checkIfUserExistAtFirestore', payload)
  },

  signOut ({ commit }) {
    firebase.auth().signOut().then(function () {
      const newUser = {}
      commit('updateUser', newUser)
      router.push('/')
    }).catch(function (error) {
      console.log(error)
      Event.$emit('notification', { message: error.message, color: 'error', timeout: 0 })
    })
  },

  signInWithGoogle ({ dispatch }) {
    var provider = new firebase.auth.GoogleAuthProvider()
    dispatch('socialSignIn', provider)
  },

  socialSignIn (context, provider) {
    context.commit('updateLoading', true)
    firebase.auth().signInWithRedirect(provider).then(function (result) {
      // var token = result.credential.accessToken
      var user = result.user
      // console.log(token, user)
      context.dispatch('checkIfUserExistAtFirestore', user)
    }).catch(function (error) {
      var errorCode = error.code
      var errorMessage = error.message
      var email = error.email
      var credential = error.credential
      console.log(errorCode, errorMessage, email, credential)
      Event.$emit('notification', { message: error.message, color: 'error', timeout: 0 })
      context.commit('updateLoading', false)
    })
  },

  signInWithEmailAndPassword (context, payload) {
    context.commit('updateLoading', true)
    firebase.auth().signInWithEmailAndPassword(payload.email, payload.password).catch(function (error) {
      var errorCode = error.code
      // var errorMessage = error.message
      console.log(errorCode)
      if (errorCode === 'auth/user-not-found') {
        context.dispatch('createUserWithEmailAndPassword', payload)
      } else {
        Event.$emit('notification', { message: error.message, color: 'error', timeout: 0 })
      }
    })
  },

  createUserWithEmailAndPassword (context, payload) {
    firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password).catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code
      // var errorMessage = error.message
      console.log(errorCode)
      Event.$emit('notification', { message: error.message, color: 'error', timeout: 0 })
    }).then(function (doc) {
      context.dispatch('initialAddToFirestore', doc)
    })
  },

  checkIfUserExistAtFirestore (context, payload) {
    var usersRef = context.state.db.collection('users').doc(payload.uid)
    usersRef.get().then(function (doc) {
      if (doc.exists) {
        context.commit('updateUser', doc.data())
        if (doc.data().isDoctor) {
          router.push('/appointments')
        }
      } else {
        context.dispatch('initialAddToFirestore', payload)
      }
      context.dispatch('syncUserAtFirestore')
      context.dispatch('getDoctors')
    }).catch(function (error) {
      console.log('Error getting document:', error)
      Event.$emit('notification', { message: error.message, color: 'error', timeout: 0 })
    })
  },

  initialAddToFirestore (context, payload) {
    var newUser = {
      isDoctor: false,
      businessHours: [],
      formComplete: false,
      uid: payload.uid,
      title: null,
      firstName: null,
      lastName: null,
      gender: null,
      dob: null,
      displayName: payload.displayName,
      phoneNumber: payload.phoneNumber,
      email: payload.email,
      photoURL: payload.photoURL,
      createdAt: payload.metadata.a,
      lastLoginAt: payload.metadata.b,
      providerID: null,
      bio: null
    }
    context.state.db.collection('users').doc(payload.uid).set(newUser).then(function () {
      console.log('Document successfully written!')
      context.commit('updateUser', newUser)
    }).catch(function (error) {
      console.error('Error writing document: ', error)
      Event.$emit('notification', { message: error.message, color: 'error', timeout: 0 })
    })
  },

  getDoctors (context) {
    context.state.db.collection('users').where('isDoctor', '==', true).onSnapshot(function (querySnapshot) {
      var docs = []
      querySnapshot.forEach(function (doc) {
        docs.push(doc.data())
      })
      context.commit('pullInDoctors', docs)
      console.log('Current doctors ', context.state.doctors.join(', '))
    })
  },

  syncUserAtFirestore (context) {
    context.state.db.collection('users').doc(context.state.user.uid).onSnapshot(function (doc) {
      let source = doc.metadata.hasPendingWrites ? 'Local' : 'Server'
      // console.log(source, ' data: ', doc && doc.data())
      console.log(source)
      context.commit('updateUser', doc.data())
    })
  },

  updateUserFieldAtFirestore (context, payload) {
    if (payload.field !== 'email') {
      var userRef = context.state.db.collection('users').doc(context.state.user.uid)
      return userRef.update(payload.field, payload.value).then(function () {
        console.log('Document successfully updated!')
      }).catch(function (error) {
        console.error('Error updating document: ', error.message)
        Event.$emit('notification', { message: error.message, color: 'error', timeout: 0 })
      })
    }
  },

  updateAppointmentFieldAtFirestore (context, payload) {
    var userRef = context.state.db.collection('appointments').doc(payload.session_id)
    return userRef.update(payload.field, payload.value).then(function () {
      console.log('Appointment successfully updated!')
    }).catch(function (error) {
      console.error('Error updating document: ', error.message)
      Event.$emit('notification', { message: error.message, color: 'error', timeout: 0 })
    })
  },

  getAppointmentsByParticipant (context) {
    context.state.db.collection('appointments').onSnapshot(function (querySnapshot) {
      var appointments = []
      querySnapshot.forEach(function (appoint) {
        let data = appoint.data()
        data.participants.forEach(function (p) {
          if (p.state === context.state.user.uid) {
            appointments.push(appoint.data())
          }
        })
      })
      // check if there are any pending requests
      if (context.state.user.isDoctor) {
        let pending = 0
        appointments.forEach(function (appoint) {
          if (appoint.status === 0 && moment(appoint.start_time).isAfter()) {
            pending++
          }
        })
        if (pending) {
          Event.$emit('notification', ({
            message: 'You have ' + pending + ' appointment requests',
            color: 'warning'
          }))
        }
      }
      // commit changes
      context.commit('updateAppointments', appointments)
      console.log('Current appointments: ', context.state.appointments)
      // raise event
      Event.$emit('appointments-refreshed', ({}))
    })
  }

}
