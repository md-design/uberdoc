export default {
  updateUser (state, user) {
    state.user = user
  },
  updateFormComplete (state, val) {
    state.formComplete = val
  },
  updateEvents: (state, events) => {
    state.events = events
  },
  updateAppointments: (state, appointments) => {
    state.appointments = appointments
  },
  pullInDoctors (state, docs) {
    state.doctors = docs
  },
  updateBooking (state, draftBooking) {
    state.draftBooking = draftBooking
  },
  updateLoading: (state, newVal) => {
    state.isLoading = newVal
  }
}
