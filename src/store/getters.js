export default {
  isLoading (state) {
    return state.isLoading
  },
  getUser (state) {
    return state.user
  },
  loginScreen (state) {
    if (Object.keys(state.user).length === 0) {
      return true
    } else {
      return false
    }
  },
  formComplete (state) {
    return state.user.formComplete
  },
  regoScreen (state) {
    if (Object.keys(state.user).length === 0) {
      return false
    } else {
      if (state.user.formComplete) {
        return false
      } else {
        return true
      }
    }
  },
  getEvents: (state) => {
    return state.events
  },
  getEventBySessionId: (state, getters) => (sessionId) => {
    return state.events.find(event => event.data.session_id === sessionId)
  },
  getAppointments: (state) => {
    return state.appointments
  },
  getDocs (state) {
    return state.doctors
  },
  getDraftBooking (state) {
    return state.draftBooking
  }
}
