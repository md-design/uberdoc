import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'
import * as firebase from 'firebase'
import 'firebase/firestore'
import config from '../../config/firebase.config'
firebase.initializeApp(config)

Vue.use(Vuex)

const state = {
  user: {},
  events: [],
  appointments: [],
  doctors: [],
  isLoading: true,
  draftBooking: {},
  db: firebase.firestore()
}

export const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})
