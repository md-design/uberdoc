// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { store } from './store/store.js'
import Vuetify from 'vuetify'
import * as firebase from 'firebase'
import FullCalendar from 'vue-full-calendar'
import { VueMaskDirective } from 'v-mask'
import VueCordova from 'vue-cordova'
import vueResource from 'vue-resource'
import momentTimezone from 'moment-timezone'
const moment = require('moment')

Vue.use(Vuetify)
Vue.use(require('vue-moment'), {
  moment,
  momentTimezone
})
Vue.directive('mask', VueMaskDirective)
Vue.config.productionTip = false
window.Event = new Vue()
window.jQuery = window.$ = require('jquery')
Vue.use(FullCalendar)
Vue.use(VueCordova)
Vue.use(vueResource)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  created () {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
      } else {
        this.$store.commit('updateLoading', false)
      }
    })
  }
})
